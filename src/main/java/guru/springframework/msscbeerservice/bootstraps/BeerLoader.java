package guru.springframework.msscbeerservice.bootstraps;

import guru.springframework.msscbeerservice.domain.Beer;
import guru.springframework.msscbeerservice.repositories.BeerRepository;
import guru.springframework.msscbeerservice.web.model.BeerStyleEnum;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

// pour que le context spring le recupere

@Component
public class BeerLoader implements CommandLineRunner {
    private final BeerRepository beerRepository;

    public BeerLoader(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        loadBeerObjects();
    }

    private void loadBeerObjects() {
        if (beerRepository.count() == 0){
            beerRepository.save(Beer.builder()
                    .beerName("Mango Bobs")
                    .beerStyle(BeerStyleEnum.valueOf("IPA"))
                    .quantityToBrew(200)
                    .minOnHand(12)
                    .upc(330456789L)
                    .price(new BigDecimal("120.95"))
                    .build());
            beerRepository.save(Beer.builder()
                    .beerName("Galaxy Cat")
                    .beerStyle(BeerStyleEnum.valueOf("PALE_ALE"))
                    .quantityToBrew(190)
                    .minOnHand(122)
                    .upc(3304449L)
                    .price(new BigDecimal("111.95"))
                    .build());
            System.out.println("empty");
        }

        System.out.println(beerRepository.count());
    }
}
